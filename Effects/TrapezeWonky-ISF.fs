/*{
	"DESCRIPTION": "Trapezoid skewing, like projector keystone, using an old wonky algorithm that results in edge curving.",
    "CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS":
    [
        {
        "NAME": "inputImage",
        "TYPE": "image"
        },
        {
            "NAME": "trapezeX",
            "LABEL": "Trapeze X",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": -1.0,
            "MAX": 1.0
        },
        {
            "NAME": "trapezeY",
            "LABEL": "Trapeze Y",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": -1.0,
            "MAX": 1.0
        },
        {
            "NAME": "textureRepeat",
            "LABEL": "Texture Repeat",
            "TYPE": "bool",
            "DEFAULT": "false"
        }
	]
 }*/

void main()
{
    vec2 pixelCoords = vv_FragNormCoord;

    // Trapeze on X axis
    pixelCoords -= vec2(0.5, 0.0);
    if (trapezeX > 0.0)
    {
        // Trapeze on top, note this requires trapezeX * -1.0
        pixelCoords.x = (pixelCoords.x / (1.0 + (-1.0 * trapezeX * pixelCoords.y)));
    }
    else
    {
        // Trapeze on bottom, image is flipped for trapezeing then flipped back
        pixelCoords.y = 1.0 - pixelCoords.y;
        pixelCoords.x = (pixelCoords.x / (1.0 + (trapezeX * pixelCoords.y)));
        pixelCoords.y = 1.0 - pixelCoords.y;
    }
    pixelCoords += vec2(0.5, 0.0);

    // Trapeze on Y axis
    pixelCoords -= vec2(0.0, 0.5);
    if (trapezeY > 0.0)
    {
        pixelCoords.y = pixelCoords.y / (1.0 + (-1.0 * trapezeY * pixelCoords.x));
    }
    else
    {
        pixelCoords.x = 1.0 - pixelCoords.x;
        pixelCoords.y = pixelCoords.y / (1.0 + (trapezeY * pixelCoords.x));
        pixelCoords.x = 1.0 - pixelCoords.x;
    }
    pixelCoords += vec2(0.0, 0.5);

    vec4 raw = IMG_NORM_PIXEL(inputImage, pixelCoords);

    if (!textureRepeat)
    {
        if (pixelCoords.x > 1.0 || pixelCoords.x < 0.0 || pixelCoords.y > 1.0 || pixelCoords.y < 0.0)
        {
            raw = vec4(0.0, 0.0, 0.0, 0.0);
        }
    }

    gl_FragColor = raw;
}