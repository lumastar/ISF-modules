/*{
	"DESCRIPTION": "performs a 3d rotation",
	"CREDIT": "Written by Wil Squires for EDWARDOtme",
	"INPUTS": [
		{
			"NAME": "inputImage",
			"TYPE": "image"
		},
		{
			"NAME": "blackBackground",
			"LABEL": "Black Background",
			"TYPE": "bool",
			"DEFAULT": "true"
		},
		{
			"NAME": "spacing",
			"LABEL": "Grid Spacing",
			"TYPE": "float",
			"MIN": 0.0,
			"MAX": 1.0,
			"DEFAULT": 0.5
		},
		{
			"NAME": "width",
			"LABEL": "Line Width",
			"TYPE": "float",
			"MIN": 0.0,
			"MAX": 1.0,
			"DEFAULT": 0.5
		},

	]
}*/

void main()
{
	vec2 pixelCoords = gl_FragCoord.xy;
	float spacingPixels = ((RENDERSIZE.x / 10.0) - (RENDERSIZE.x / 2.0)) * spacing + (RENDERSIZE.x / 2.0);
	
	float a = (pixelCoords.x - (width / 2.0)) / spacingPixels;
	a -= floor(a);
	
	vec4 pixelColor = vec4(0.0, 0.0, 0.0, 1.0);
	
	if (a < width)
		pixelColor = vec4(1.0, 1.0, 1.0, 1.0);
		
	float b = (pixelCoords.y / spacingPixels);
	b -= floor(b);
	
	if ((b - (width / 2.0)) < width)
		pixelColor = vec4(1.0, 1.0, 1.0, 1.0);
	
	gl_FragColor = pixelColor;
}