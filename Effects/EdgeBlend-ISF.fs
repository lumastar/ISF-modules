/*{
	"DESCRIPTION": "Module uses Interactive Shader Format, implemented with just a fragment shader.",
	"CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS": [
		{
			"NAME": "inputImage",
			"TYPE": "image"
		},
		{
			"NAME": "edge",
			"VALUES": [
				0,
				1,
				2,
				3
			],
			"LABELS": [
				"Top",
				"Right",
				"Bottom",
				"Left"
			],
			"DEFAULT": 1,
			"TYPE": "long"
		},
		{
			"NAME": "pixels",
			"LABEL": "Pixels",
			"TYPE": "float",
			"DEFAULT": 100.0,
			"MIN": 1.0,
			"MAX": 2000.0
		},
		{
			"NAME": "angle",
			"LABEL": "Angle",
			"TYPE": "float",
			"DEFAULT": 0.0,
			"MIN": -1.0,
			"MAX": 1.0
		},
		{
			"NAME": "fade",
			"LABEL": "Fade",
			"TYPE": "float",
			"DEFAULT": 0.5,
			"MIN": 0.0,
			"MAX": 1.0
		},
		{
			"NAME": "curve",
			"LABEL": "Curve",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
			"MAX": 10.0
		},
		{
			"NAME": "minRed",
			"LABEL": "Min Red",
			"TYPE": "float",
			"DEFAULT": 0.0,
			"MIN": 0.0,
			"MAX": 1.0
		},
		{
			"NAME": "minGreen",
			"LABEL": "Min Green",
			"TYPE": "float",
			"DEFAULT": 0.0,
			"MIN": 0.0,
			"MAX": 1.0
		},
		{
			"NAME": "minBlue",
			"LABEL": "Min Blue",
			"TYPE": "float",
			"DEFAULT": 0.0,
			"MIN": 0.0,
			"MAX": 1.0
		},
		{
			"NAME": "maxRed",
			"LABEL": "Max Red",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
			"MAX": 1.0
		},
		{
			"NAME": "maxGreen",
			"LABEL": "Max Green",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
			"MAX": 1.0
		},
		{
			"NAME": "maxBlue",
			"LABEL": "Max Blue",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
			"MAX": 1.0
		},
		{
			"NAME": "gammaRed",
			"LABEL": "Gamma Red",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
			"MAX": 10.0
		},
		{
			"NAME": "gammaGreen",
			"LABEL": "Gamma Green",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
			"MAX": 10.0
		},
		{
			"NAME": "gammaBlue",
			"LABEL": "Gamma Blue",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
			"MAX": 10.0
		}
	]
}*/

void main()
{
    vec4 pixelColor = IMG_THIS_PIXEL(inputImage);
    float blend;
    float angleScale = 0.1;
    if (edge == 0)
    {
    	blend = RENDERSIZE.y - (vv_FragNormCoord.y * RENDERSIZE.y) + ((vv_FragNormCoord.x * RENDERSIZE.x - (RENDERSIZE.x * 0.5)) * angle * angleScale);
    }
    else if (edge == 1)
    {
    	blend = RENDERSIZE.x - (vv_FragNormCoord.x * RENDERSIZE.x) + ((vv_FragNormCoord.y * RENDERSIZE.y - (RENDERSIZE.y * 0.5)) * angle * angleScale);
    }
    else if (edge == 2)
    {
    	blend = vv_FragNormCoord.y * RENDERSIZE.y + ((vv_FragNormCoord.x * RENDERSIZE.x - (RENDERSIZE.x * 0.5)) * angle * angleScale);
    }
    else
    {
    	blend = vv_FragNormCoord.x * RENDERSIZE.x + ((vv_FragNormCoord.y * RENDERSIZE.y - (RENDERSIZE.y * 0.5)) * angle * angleScale);
    }
    // Determine blend amount based on pixel position and fade amount
	blend = (((blend / pixels) * pow(fade, curve)) + (1.0 - fade));
	// Cap blend value at 1.0
	blend = blend > 1.0 ? 1.0 : blend;
	// Scale pixel values by blend value to reduce brightness in blend area
	pixelColor.xyz = pixelColor.xyz * vec3(blend);
	// Increase black levels in non blend area by adding min values to RGB
    pixelColor.xyz = blend < 1.0 ? pixelColor.xyz : pixelColor.xyz + vec3(minRed, minGreen, minBlue);
    // Decrease white level in blend area by scaling RGB by max values
    pixelColor.xyz = blend == 1.0 ? pixelColor.xyz : pixelColor.xyz * vec3(maxRed, maxGreen, maxBlue);
    // Apply gamma to blend area
    pixelColor.xyz = blend < 1.0 ? vec3(pow(pixelColor.x, gammaRed), pow(pixelColor.y, gammaGreen), pow(pixelColor.z, gammaBlue)) : pixelColor.xyz;
    gl_FragColor = pixelColor;
}