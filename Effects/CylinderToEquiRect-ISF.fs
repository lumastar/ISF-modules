/*{
	"DESCRIPTION": "",
	"CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS": [
		{
			"NAME": "inputCylinder",
			"TYPE": "image"
		}
	]
	
}*/

void main()	{
	float PI = 3.141592653589793;
	
	// Represent this pixels cartesian coords as spherical coords
	float phi = vv_FragNormCoord.x * 2.0 * PI;
	float theta = vv_FragNormCoord.y * PI;
	
	// Calculate 3D cartesian coords from spherical coords
	// ?? What is the * -1.0 part doing tho yh??
	float x = sin(phi) * sin(theta) * -1.0;
	float y = cos(theta);
	float z = cos(phi) * sin(theta) * -1.0;
	
	// Find the component (x, y or z) of the coords with largest magnitude
	float a = max(abs(x),
		( 
			max(abs(y), abs(z))
		)
	);
	
	// Divide through by largest component to give one component equal to
	// ±1 and the others between ±1
	float xa = x/a;
	float ya = y/a;
	float za = z/a;
	
	float cubeFaceWidth = RENDERSIZE.x / 4.0;
	float cubeFaceHeight = RENDERSIZE.y / 2.0;
	
	vec2 pixelCoords;
	
	if (ya > 0.99)
    {
        // Bottom
        pixelCoords.x = ((xa + 1.0) / 2.0) * RENDERSIZE.x;
        pixelCoords.y = ((za + 1.0) / 2.0) * (0.25 * RENDERSIZE.y);
        float x = 0.125 * RENDERSIZE.x;
        pixelCoords.x = mod((pixelCoords.x + x), RENDERSIZE.x);
		pixelCoords.y += (0.0 * RENDERSIZE.y);
    }
    else if (ya < -0.99)
    {
        // Top
        pixelCoords.x = ((xa + 1.0) / 2.0) * RENDERSIZE.x;
        pixelCoords.y = ((za + 1.0) / 2.0) * (0.25 * RENDERSIZE.y);
        float x = 0.125 * RENDERSIZE.x;
        pixelCoords.x = mod((pixelCoords.x + x), RENDERSIZE.x);
		pixelCoords.y += (0.75 * RENDERSIZE.y);
    }
	else if (xa > 0.99)
	{
		// Right
		// ?? need the * -1 to correct texture flip	
		pixelCoords.x = (((za * -1.0) + 1.0) / 2.0) * cubeFaceWidth;
		pixelCoords.y = ((ya + 1.0) / 2.0) * cubeFaceHeight;
		pixelCoords.x += (0.5 + 0.125) * RENDERSIZE.x;
		pixelCoords.y += (0.25 * RENDERSIZE.y);
	}
	else if (xa < -0.99)
    {
        //Left
        pixelCoords.x = ((za + 1.0) / 2.0) * cubeFaceWidth;
        pixelCoords.y = ((ya + 1.0) / 2.0) * cubeFaceHeight;
		pixelCoords.x += (0.0 + 0.125) * RENDERSIZE.x;
		pixelCoords.y += (0.25 * RENDERSIZE.y);
    }
    else if (za > 0.99)
    {
        //Front
        pixelCoords.x = ((xa + 1.0) / 2.0) * cubeFaceWidth;
        pixelCoords.y = ((ya + 1.0) / 2.0) * cubeFaceHeight;
		pixelCoords.x += (0.25 + 0.125) * RENDERSIZE.x;
		pixelCoords.y += (0.25 * RENDERSIZE.y);
        
    }
    else if (za < -0.99)
    {
        // Back
        pixelCoords.x = (((xa * -1.0) + 1.0) / 2.0) * cubeFaceWidth;
        pixelCoords.y = ((ya + 1.0) / 2.0) * cubeFaceHeight;
        float x = (0.75 + 0.125) * RENDERSIZE.x;
        pixelCoords.x = mod((pixelCoords.x + x), RENDERSIZE.x);
		pixelCoords.y += (0.25 * RENDERSIZE.y);
    }
    
	vec4 pixelColour = IMG_PIXEL(inputCylinder, pixelCoords);
	
	gl_FragColor = pixelColour;
}
