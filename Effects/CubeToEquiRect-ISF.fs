/*{
	"DESCRIPTION": "",
	"CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS": [
		{
			"NAME": "inputRight",
			"TYPE": "image"
		},
		{
			"NAME": "inputLeft",
			"TYPE": "image"
		},
		{
			"NAME": "inputUp",
			"TYPE": "image"
		},
		{
			"NAME": "inputDown",
			"TYPE": "image"
		},
		{
			"NAME": "inputFront",
			"TYPE": "image"
		},
		{
			"NAME": "inputBack",
			"TYPE": "image"
		}
	]
	
}*/

void main()	{
	float PI = 3.141592653589793;
	
	float cubeFaceWidth;
	float cubeFaceWidthOffset;
	float cubeFaceHeight;
	float cubeFaceHeightOffset;
	
	if (RENDERSIZE.x > RENDERSIZE.y)
	{
		cubeFaceWidth = RENDERSIZE.y;
		cubeFaceWidthOffset = (RENDERSIZE.x - RENDERSIZE.y) / 2.0;
		cubeFaceHeight = RENDERSIZE.y;
		cubeFaceHeightOffset = 0.0;
	}
	else
	{
		cubeFaceWidth = RENDERSIZE.x;
		cubeFaceWidthOffset = 0.0;
		cubeFaceHeight = RENDERSIZE.x;
		cubeFaceHeightOffset = (RENDERSIZE.y - RENDERSIZE.x) / 2.0;
	}
	
	float u = (gl_FragCoord.x / RENDERSIZE.x);
	float phi = u * 2.0 * PI;
	
	float v = 1.0 - (gl_FragCoord.y / RENDERSIZE.y);
	float theta = v * PI;
	
	float x = sin(phi) * sin(theta) * -1.0;
	float y = cos(theta);
	float z = cos(phi) * sin(theta) * -1.0;
	
	float a = max(abs(x),
		( 
			max(abs(y), abs(z))
		)
	);
	
	float xa = x/a;
	float ya = y/a;
	float za = z/a;
	
	vec4 pixelColour;
	vec2 pixelCoords;

	if (xa > 0.99)
	{
		//Right
		pixelCoords.x = ((((za + 1.0) / 2.0)) * cubeFaceWidth);
		pixelCoords.y = ((((ya + 1.0) / 2.0)) * cubeFaceHeight);
		pixelCoords.x += cubeFaceWidthOffset;
		pixelCoords.y += cubeFaceHeightOffset;
		pixelColour = IMG_PIXEL(inputRight, pixelCoords);
	}
	else if (xa < -0.99)
    {
        //Left
        pixelCoords.x = ((((za + 1.0) / 2.0)) * cubeFaceWidth);
        pixelCoords.y = ((((ya + 1.0) / 2.0)) * cubeFaceHeight);
        pixelCoords.x += cubeFaceWidthOffset;
		pixelCoords.y += cubeFaceHeightOffset;
        pixelColour = IMG_PIXEL(inputLeft, pixelCoords);
    }
    else if (ya > 0.99)
    {
        //Up
        pixelCoords.x = ((((xa + 1.0) / 2.0)) * cubeFaceWidth);
        pixelCoords.y = ((((za + 1.0) / 2.0) - 1.0) * cubeFaceHeight);
        pixelCoords.x += cubeFaceWidthOffset;
		pixelCoords.y += cubeFaceHeightOffset;
        pixelColour = IMG_PIXEL(inputUp, pixelCoords);
    }
    else if (ya < -0.99)
    {
        //Down
        pixelCoords.x = ((((xa + 1.0) / 2.0)) * cubeFaceWidth);
        pixelCoords.y = ((((za + 1.0) / 2.0)) * cubeFaceHeight);
        pixelCoords.x += cubeFaceWidthOffset;
		pixelCoords.y += cubeFaceHeightOffset;
        pixelColour = IMG_PIXEL(inputDown, pixelCoords);
    }
    else if (za > 0.99)
    {
        //Front
        pixelCoords.x = ((((xa + 1.0) / 2.0)) * cubeFaceWidth);
        pixelCoords.y = ((((ya + 1.0) / 2.0)) * cubeFaceHeight);
        pixelCoords.x += cubeFaceWidthOffset;
		pixelCoords.y += cubeFaceHeightOffset;
        pixelColour = IMG_PIXEL(inputFront, pixelCoords);
    }
    else if (za < -0.99)
    {
        //Back
        pixelCoords.x = ((((xa + 1.0) / 2.0)) * cubeFaceWidth);
        pixelCoords.y = ((((ya + 1.0) / 2.0)) * cubeFaceHeight);
        pixelCoords.x += cubeFaceWidthOffset;
		pixelCoords.y += cubeFaceHeightOffset;
        pixelColour = IMG_PIXEL(inputBack, pixelCoords);
    }
    else
    {
        //Unknown face, something went wrong
        pixelColour = vec4(1.0, 0.0, 0.0, 0.0);
    }
	
	gl_FragColor = pixelColour;
}
