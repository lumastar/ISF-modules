/*{
	"DESCRIPTION": "Cylindrically warp the input image. Works by projecting the image onto a cylinder with adjustable focus distance and radius. Currently the cylinder can only be concave.",
	"CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS": [
		{
			"NAME": "inputImage",
			"TYPE": "image"
		},
		{
			"NAME": "f",
			"LABEL": "Focus",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
			"MAX": 10.0
		},
		{
			"NAME": "r",
			"LABEL": "Radius",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.5,
			"MAX": 10.0
		},
		{
			"NAME": "angle",
			"LABEL": "Angle",
			"TYPE": "float",
			"DEFAULT": 0.0,
			"MIN": 0.0,
			"MAX": 1.0
		},
	]
	
}*/

void main()	{
	vec4 pixelColor;
	
	// this pixels coords
	vec2 pixelCoords = vv_FragNormCoord;
	
	// shift origin from bottom left to centre of image
	pixelCoords -= vec2(0.5, 0.5);
	
	// Convert angle to half radians
    float theta = angle * 3.14159;
    
    // Calculate aspect ratio for scaling to avoid rotation being skewed
    float aspect = (RENDERSIZE.x / RENDERSIZE.y);
    
    // Scale down based on aspect ratio
    pixelCoords *= vec2(aspect, 1.0);

    // Rotate
    pixelCoords = vec2(
        (pixelCoords.x * cos(theta) - pixelCoords.y * sin(theta)),
        (pixelCoords.x * sin(theta) + pixelCoords.y * cos(theta))
    );
	
	float c = pixelCoords.x;
	// is this a vertical or horizonatal warp?
	// warp maps input image to cylinder based on circle in xz or yz plane
	// vertical is yz plane
	// horizonatal is xz plane
	// this is now redundant as the angle parameter can be used
	//if (axis == 0)
	//	c = pixelCoords.x;
	//else
	//	c = pixelCoords.y;

	// calculate the origin of the circle in the xz or yz plane
	float z0 = f - sqrt(r*r-0.25);
	
	// somewhere i need to alter the size of the cylnder to account for aspect skew things
	
	// work out z coord on cylinder
	float zc = (2.0*z0+sqrt(4.0*z0*z0-4.0*(c*c/(f*f)+1.0)*(z0*z0-r*r)))/(2.0* (c*c/(f*f)+1.0));
	
	// set pixel coords in original image
	pixelCoords = vec2(pixelCoords.x*zc/f,pixelCoords.y*zc/f);
	
	// Rotate back
    pixelCoords = vec2(
        ( pixelCoords.x * cos(theta) + pixelCoords.y * sin(theta)),
        (-pixelCoords.x * sin(theta) + pixelCoords.y * cos(theta))
    );
    
   	// Scale back up
    pixelCoords *= vec2((1.0 / aspect), 1.0);
    
	// reset origin
	pixelCoords += vec2(0.5, 0.5);
	
	// get this pixel color from pixel in original image
	pixelColor = IMG_NORM_PIXEL(inputImage, pixelCoords);
	
	// stop texture repeating
	if (pixelCoords.x > 1.0 || pixelCoords.x < 0.0 || pixelCoords.y > 1.0 || pixelCoords.y < 0.0)
       pixelColor = vec4(0.0, 0.0, 0.0, 0.0);
   
	gl_FragColor = pixelColor;
}
