/*{
	"DESCRIPTION": "2D scaling on custom rotated axis using Interactive Shader Format, implemeted using a vertex and fragment shader.",
    "CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS":
    [
        {
        "NAME": "inputImage",
        "TYPE": "image"
        },
        {
            "NAME": "rotation",
            "LABEL": "Rotation",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": -1.0,
            "MAX": 1.0
        },
        {
            "NAME": "scaleX",
            "LABEL": "Scale X",
            "TYPE": "float",
            "DEFAULT": 1.0
        },
        {
            "NAME": "scaleY",
            "LABEL": "Scale Y",
            "TYPE": "float",
            "DEFAULT": 1.0
        }
	]
 }*/

void main()
{

    gl_FragColor = IMG_THIS_PIXEL(inputImage);
    
}