/*{
	"DESCRIPTION": "performs a 3d rotation",
	"CREDIT": "Original by zoidberg, modified by Wil Squires for EDWARDOtme",
	"INPUTS": [
		{
			"NAME": "inputImage",
			"TYPE": "image"
		},
		{
			"NAME": "xrot",
			"LABEL": "X Rotate",
			"TYPE": "float",
			"MIN": -1.0,
			"MAX": 1.0,
			"DEFAULT": 0.0
		},
		{
			"NAME": "yrot",
			"LABEL": "Y Rotate",
			"TYPE": "float",
			"MIN": -1.0,
			"MAX": 1.0,
			"DEFAULT": 0.0
		},
		{
			"NAME": "zrot",
			"LABEL": "Z Rotate",
			"TYPE": "float",
			"MIN": -1.0,
			"MAX": 1.0,
			"DEFAULT": 0.0
		},
		{
			"NAME": "zoom",
			"LABEL": "Zoom Level",
			"TYPE": "float",
			"MIN": 0.0,
			"MAX": 1.0,
			"DEFAULT": 1.0
		},
		{
			"NAME": "perspective",
			"LABEL": "Perspective",
			"TYPE": "bool",
			"DEFAULT": true
		}
	]
}*/


void main()
{
	gl_FragColor = IMG_THIS_PIXEL(inputImage);
}
