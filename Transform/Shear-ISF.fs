/*{
	"DESCRIPTION": "2D shearing using Interactive Shader Format, implemeted using a vertex and fragment shader.",
    "CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS":
    [
        {
        "NAME": "inputImage",
        "TYPE": "image"
        },
        {
            "NAME": "shearX",
            "LABEL": "Shear X",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": -1.0,
            "MAX": 1.0
        },
        {
            "NAME": "shearY",
            "LABEL": "Shear Y",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": -1.0,
            "MAX": 1.0
        }
	]
 }*/

void main()
{

    gl_FragColor = IMG_THIS_PIXEL(inputImage);
    
}