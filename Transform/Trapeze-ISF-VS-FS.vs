float		PI_CONST = 3.14159265359;

//	with help from http://duriansoftware.com/joe/An-intro-to-modern-OpenGL.-Chapter-3:-3D-transformation-and-projection.html
//	and http://en.wikipedia.org/wiki/Rotation_matrix

mat4 view_frustum(
    float angle_of_view,
    float aspect_ratio,
    float z_near,
    float z_far
) {
    return mat4(
        vec4(1.0/tan(angle_of_view),           0.0, 0.0, 0.0),
        vec4(0.0, aspect_ratio/tan(angle_of_view),  0.0, 0.0),
        vec4(0.0, 0.0,    (z_far+z_near)/(z_far-z_near), 1.0),
        vec4(0.0, 0.0, -2.0*z_far*z_near/(z_far-z_near), 0.0)
    );
}

mat4 scale(float x, float y, float z)
{
    return mat4(
        vec4(x,   0.0, 0.0, 0.0),
        vec4(0.0, y,   0.0, 0.0),
        vec4(0.0, 0.0, z,   0.0),
        vec4(0.0, 0.0, 0.0, 1.0)
    );
}

mat4 translate(float x, float y, float z)
{
    return mat4(
        vec4(1.0, 0.0, 0.0, 0.0),
        vec4(0.0, 1.0, 0.0, 0.0),
        vec4(0.0, 0.0, 1.0, 0.0),
        vec4(x,   y,   z,   1.0)
    );
}

mat4 rotate_x(float theta)
{
    return mat4(
        vec4(1.0, 0.0, 0.0, 0.0),
        vec4(0.0, cos(theta*PI_CONST), sin(theta*PI_CONST), 0.0),
        vec4(0.0, -sin(theta*PI_CONST), cos(theta*PI_CONST), 0.0),
        vec4(0.0, 0.0, 0.0, 1.0)
    );
}

mat4 rotate_y(float theta)
{
    return mat4(
        vec4(cos(theta*PI_CONST), 0.0, sin(theta*PI_CONST), 0.0),
        vec4(0.0, 1.0, 0.0, 0.0),
        vec4(-sin(theta*PI_CONST), 0, cos(theta*PI_CONST), 0.0),
        vec4(0.0, 0.0, 0.0, 1.0)
    );
}

mat4 rotate_z(float theta)
{
    return mat4(
        vec4(cos(theta*PI_CONST), -sin(theta*PI_CONST), 0.0, 0.0),
        vec4(sin(theta*PI_CONST), cos(theta*PI_CONST), 0.0, 0.0),
        vec4(0.0, 0.0, 1.0, 0.0),
        vec4(0.0, 0.0, 0.0, 1.0)
    );
}

void main()
{
	vv_vertShaderInit();
	
	vec4 position = gl_Position;
    float rotscale = 0.125;

    float xback = abs(sin(xrot * PI_CONST * rotscale));
    float yback = abs(sin(yrot * PI_CONST * rotscale)) * (RENDERSIZE.x/RENDERSIZE.y);
    float back = xback + yback;

    float xshift = yback * 0.5 * tan(yrot * PI_CONST * rotscale) * (RENDERSIZE.x/RENDERSIZE.y);
    float yshift = xback * 0.5 * tan(xrot * PI_CONST * rotscale);

    position = view_frustum(radians(45.0), RENDERSIZE.x/RENDERSIZE.y, 0.0, 2.0)
    * translate(0.0, 0.0, RENDERSIZE.x/RENDERSIZE.y)
    * translate(xshift/(RENDERSIZE.x/RENDERSIZE.y), yshift, back)
    * rotate_x(-xrot * rotscale)
    * rotate_y(-yrot * rotscale)
    * scale(RENDERSIZE.x/RENDERSIZE.y, 1.0, 1.0)
    * position;

    gl_Position = position;
}
