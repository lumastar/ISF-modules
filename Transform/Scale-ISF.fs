/*{
	"DESCRIPTION": "2D scaling using Interactive Shader Format, implemented with vertex shader.\n\nX:\nThe amount to scale on the X axis.\nY:\nThe amount to scale on the Y axis.",
	"INPUTS":
    [
        {
        "NAME": "inputImage",
        "TYPE": "image"
        },
        {
            "NAME": "scaleX",
            "LABEL": "X",
            "TYPE": "float",
            "DEFAULT": 1.0
        },
        {
            "NAME": "scaleY",
            "LABEL": "Y",
            "TYPE": "float",
            "DEFAULT": 1.0
        }
	]
 }*/

void main()
{
    gl_FragColor = IMG_THIS_PIXEL(inputImage);
}