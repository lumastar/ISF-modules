/*{
	"DESCRIPTION": "Trapezoid skewing, much like keystone correction featured on projectors. Positive and negative values trapeze in the opposite way on each axis.",
    "CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS":
    [
        {
        "NAME": "inputImage",
        "TYPE": "image"
        },
        {
            "NAME": "xrot",
            "LABEL": "Trapeze X",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": -1.0,
            "MAX": 1.0
        },
        {
            "NAME": "yrot",
            "LABEL": "Trapeze Y",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": -1.0,
            "MAX": 1.0
        }
	]
 }*/

void main()
{

    gl_FragColor = IMG_THIS_PIXEL(inputImage);

}