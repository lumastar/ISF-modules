mat4 shear(float x, float y)
{
    return mat4(
        vec4(1.0, y,   0.0, 0.0),
        vec4(x,   1.0, 0.0, 0.0),
        vec4(0.0, 0.0, 1.0, 0.0),
        vec4(0.0, 0.0, 1.0, 1.0)
    );
}

void main()
{
    vv_vertShaderInit();
    
    vec4 position = gl_Position;

    gl_Position = shear(shearX, shearY) * position;
}