/*{
	"DESCRIPTION": "Trapezoid skewing, much like keystone correction featured on projectors. Positive and negative values trapeze in the opposite way on each axis.",
    "CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS":
    [
        {
        "NAME": "inputImage",
        "TYPE": "image"
        },
        {
            "NAME": "trapezeX",
            "LABEL": "Trapeze X",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": -1.0,
            "MAX": 1.0
        },
        {
            "NAME": "trapezeY",
            "LABEL": "Trapeze Y",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": -1.0,
            "MAX": 1.0
        }
	]
 }*/

void main()
{
    // this pixels coords
    vec2 pixelCoords = vv_FragNormCoord;
    
    // shift origin from bottom left to centre of image
    pixelCoords -= vec2(0.5, 0.5);
    
    float y = (-1.0 * trapezeX) * pixelCoords.x + (1.0 - 0.5 * sqrt(trapezeX * trapezeX));
    float x = (-1.0 * trapezeY) * pixelCoords.y + (1.0 - 0.5 * sqrt(trapezeY * trapezeY));
    
    pixelCoords /= vec2(x, y);

    // reset origin
    pixelCoords += vec2(0.5, 0.5);

    vec4 pixelColor = IMG_NORM_PIXEL(inputImage, pixelCoords);

    if (pixelCoords.x > 1.0 || pixelCoords.x < 0.0 || pixelCoords.y > 1.0 || pixelCoords.y < 0.0)
    	pixelColor = vec4(0.0, 0.0, 0.0, 0.0);

    gl_FragColor = pixelColor;
}