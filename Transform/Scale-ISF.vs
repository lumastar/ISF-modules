mat4 scale(float x, float y)
{
    return mat4(
        vec4(x,   0.0, 0.0, 0.0),
        vec4(0.0, y,   0.0, 0.0),
        vec4(0.0, 0.0, 1.0, 0.0),
        vec4(0.0, 0.0, 0.0, 1.0)
    );
}

void main()
{
    vv_vertShaderInit();
    
    vec4 position = gl_Position;

    gl_Position = scale(scaleX, scaleY) * position;

}