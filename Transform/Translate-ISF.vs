mat4 translate(float x, float y, float z)
{
    return mat4(
        vec4(1.0, 0.0, 0.0, 0.0),
        vec4(0.0, 1.0, 0.0, 0.0),
        vec4(0.0, 0.0, 1.0, 0.0),
        vec4(x,   y,     z, 1.0)
    );
}

void main()
{

    vv_vertShaderInit();
    
    vec4 position = gl_Position;

    gl_Position = translate(translateX/(RENDERSIZE.x/RENDERSIZE.y), translateY, translateZ) * position;

}