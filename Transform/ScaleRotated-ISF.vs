float PI_CONST = 3.14159265359;

mat4 rotate_z(float theta)
{
    return mat4(
        vec4(cos(theta*PI_CONST), -sin(theta*PI_CONST), 0.0, 0.0),
        vec4(sin(theta*PI_CONST), cos(theta*PI_CONST), 0.0, 0.0),
        vec4(0.0, 0.0, 1.0, 0.0),
        vec4(0.0, 0.0, 0.0, 1.0)
    );
}

mat4 scale(float x, float y)
{
    return mat4(
        vec4(x,   0.0, 0.0, 0.0),
        vec4(0.0, y,   0.0, 0.0),
        vec4(0.0, 0.0, 1.0, 0.0),
        vec4(0.0, 0.0, 0.0, 1.0)
    );
}

void main()
{
    vv_vertShaderInit();
    
    vec4 position = gl_Position;

    gl_Position = scale(1.0/(RENDERSIZE.x/RENDERSIZE.y), 1.0) 
    * rotate_z(rotation * 2.0) 
    * scale(scaleX, scaleY) 
    * rotate_z(-rotation * 2.0) 
    * scale((RENDERSIZE.x/RENDERSIZE.y), 1.0) 
    * position;
}