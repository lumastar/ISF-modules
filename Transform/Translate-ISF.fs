/*{
    "DESCRIPTION": "2D translation using Interactive Shader Format, implemeted using vertex and fragment shader",
    "CREDIT": "written by Wil Squires for EDWARDOtme",
    "INPUTS":
    [
        {
        "NAME": "inputImage",
        "TYPE": "image"
        },
        {
            "NAME": "translateX",
            "LABEL": "Translate X",
            "TYPE": "float",
            "DEFAULT": 0.0
        },
        {
            "NAME": "translateY",
            "LABEL": "Translate Y",
            "TYPE": "float",
            "DEFAULT": 0.0
        },
        {
            "NAME": "translateZ",
            "LABEL": "Translate Z",
            "TYPE": "float",
            "DEFAULT": 0.0
        }
    ]
 }*/

void main()
{

    gl_FragColor = IMG_THIS_PIXEL(inputImage);

}