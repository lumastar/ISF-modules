/*{
	"DESCRIPTION": "Separate hue shifting of red, green and blue. Module is 2D only and uses Interactive Shader Format, impemented with just a fragment shader.",
    "CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS":
    [
        {
        "NAME": "inputImage",
        "TYPE": "image"
        },
        {
            "NAME": "hueShiftRed",
            "LABEL": "Hue Shift Red",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "hueShiftGreen",
            "LABEL": "Hue Shift Green",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "hueShiftBlue",
            "LABEL": "Hue Shift Blue",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "Scale",
            "TYPE": "bool"
        }
	]
 }*/

void main()
{
    float cosR = cos(hueShiftRed * 2.0 * 3.14159);
    float sinR = sin(hueShiftRed * 2.0 * 3.14159);

    float cosG = cos(hueShiftGreen * 2.0 * 3.14159);
    float sinG = sin(hueShiftGreen * 2.0 * 3.14159);

    float cosB = cos(hueShiftBlue * 2.0 * 3.14159);
    float sinB = sin(hueShiftBlue * 2.0 * 3.14159);

    vec4 inputRaw = IMG_THIS_PIXEL(inputImage);

    float brightness = inputRaw.x + inputRaw.y + inputRaw.z;

    vec4 raw = vec4(
        (
            inputRaw.x * (cosR + (1.0 - cosR) / 3.0) +
            inputRaw.y * (1.0 / 3.0 * (1.0 - cosG) - sqrt(1.0 / 3.0) * sinG) +
            inputRaw.z * (1.0 / 3.0 * (1.0 - cosB) + sqrt(1.0 / 3.0) * sinB)
        ),
        (
            inputRaw.x * (1./3. * (1.0 - cosR) + sqrt(1.0/3.0) * sinR) +
            inputRaw.y * (cosG + 1.0 / 3.0 * (1.0 - cosG)) +
            inputRaw.z * (1.0 / 3.0 * (1.0 - cosB) - sqrt(1.0 / 3.0) * sinB)
        ),
        (
            inputRaw.x * (1.0 / 3.0 * (1.0 - cosR) - sqrt(1.0 / 3.0) * sinR) +
            inputRaw.y * (1.0 / 3.0 * (1.0 - cosG) + sqrt(1.0 / 3.0) * sinG) +
            inputRaw.z * (cosB + 1.0 / 3.0 * (1.0 - cosB))
        ),
        inputRaw.w
    );

    if (Scale)
    {
        float newBrightness = raw.x + raw.y + raw.z;
        if (newBrightness > brightness)
            raw *= (1.0 / (newBrightness - brightness));
    }

    gl_FragColor = raw;
}