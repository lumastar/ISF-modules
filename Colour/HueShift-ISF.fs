/*{
	"DESCRIPTION": "Hue shifting. Module is 2D only and uses Interactive Shader Format, impemented with just a fragment shader.",
    "CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS":
    [
        {
        "NAME": "inputImage",
        "TYPE": "image"
        },
        {
            "NAME": "hueShift",
            "LABEL": "Hue Shift",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": 0.0,
            "MAX": 1.0
        }
	]
 }*/

void main()
{
    float cosA = cos(hueShift * 2.0 * 3.14159);
    float sinA = sin(hueShift * 2.0 * 3.14159);

    vec4 inputRaw = IMG_THIS_PIXEL(inputImage);

    float brightness = inputRaw.x + inputRaw.y + inputRaw.z;

    vec4 raw = vec4(
        (
            inputRaw.x * (cosA + (1.0 - cosA) / 3.0) +
            inputRaw.y * (1.0 / 3.0 * (1.0 - cosA) - sqrt(1.0 / 3.0) * sinA) +
            inputRaw.z * (1.0 / 3.0 * (1.0 - cosA) + sqrt(1.0 / 3.0) * sinA)
        ),
        (
            inputRaw.x * (1./3. * (1.0 - cosA) + sqrt(1.0/3.0) * sinA) +
            inputRaw.y * (cosA + 1.0 / 3.0 * (1.0 - cosA)) +
            inputRaw.z * (1.0 / 3.0 * (1.0 - cosA) - sqrt(1.0 / 3.0) * sinA)
        ),
        (
            inputRaw.x * (1.0 / 3.0 * (1.0 - cosA) - sqrt(1.0 / 3.0) * sinA) +
            inputRaw.y * (1.0 / 3.0 * (1.0 - cosA) + sqrt(1.0 / 3.0) * sinA) +
            inputRaw.z * (cosA + 1.0 / 3.0 * (1.0 - cosA))
        ),
        inputRaw.w
    );

    gl_FragColor = raw;
}