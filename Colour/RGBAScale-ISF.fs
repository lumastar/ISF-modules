/*{
	"DESCRIPTION": "Scale the red, green, blue and alpha values. The multiplier values have no limit however the final red, green, blue and alpha values are bounded between 0 and 1. Apply alpha multiplies the output red, green and blue values by the alpha value for pre-multiplied aplha. Module uses Interactive Shader Format, implemented with just a fragment shader.",
	"CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS": [
		{
			"NAME": "inputImage",
			"TYPE": "image"
		}	,
		{
			"NAME": "red",
			"LABEL": "Red",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
		},
		{
			"NAME": "green",
			"LABEL": "Green",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
		},
		{
			"NAME": "blue",
			"LABEL": "Blue",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
		},
		{
			"NAME": "alpha",
			"LABEL": "Alpha",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
		},
        {
            "NAME": "applyAlpha",
            "LABEL": "Apply alpha",
            "TYPE": "bool",
            "DEFAULT": "false"
        }
	]
}*/

void main()
{
    vec4 raw = IMG_THIS_PIXEL(inputImage);
	float alpha = raw.w * alpha > 1.0 ? 1.0 : raw.w * alpha;
	float red = raw.x * red > 1.0 ? 1.0 : raw.x * red;
	float green = raw.y * green > 1.0 ? 1.0 : raw.y * green;
	float blue = raw.z * blue > 1.0 ? 1.0 : raw.z * blue;
	
	
    if (applyAlpha)
        gl_FragColor = vec4 (red * alpha, green * alpha, blue * alpha, alpha);
    else
        gl_FragColor = vec4 (red, green, blue, alpha);
}