/*{
	"DESCRIPTION": "Create solid colour.",
	"CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS": [
		{
			"NAME": "red",
			"LABEL": "Red",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
		},
		{
			"NAME": "green",
			"LABEL": "Green",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
		},
		{
			"NAME": "blue",
			"LABEL": "Blue",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
		},
		{
			"NAME": "alpha",
			"LABEL": "Alpha",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
		},
        {
            "NAME": "applyAlpha",
            "LABEL": "Apply alpha",
            "TYPE": "bool",
            "DEFAULT": "false"
        }
	]
}*/

void main()
{
    if (applyAlpha)
        gl_FragColor = vec4(red * alpha, green * alpha, blue * alpha, alpha);
    else
        gl_FragColor = vec4(red, green, blue, alpha);
}