/*{
	"DESCRIPTION": "Change the red, green and blue values. Values from 0 to 1 scale whereas values greater than 1 are additive. Module uses Interactive Shader Format, implemented with just a fragment shader.",
	"CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS": [
		{
			"NAME": "inputImage",
			"TYPE": "image"
		},
		{
			"NAME": "red",
			"LABEL": "Red",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
			"MAX": 2.0
		},
		{
			"NAME": "green",
			"LABEL": "Green",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
			"MAX": 2.0
		},
		{
			"NAME": "blue",
			"LABEL": "Blue",
			"TYPE": "float",
			"DEFAULT": 1.0,
			"MIN": 0.0,
			"MAX": 2.0
		}
	]
}*/

void main()
{
    vec4 colorScale = vec4(red, green, blue, 1.0);
    vec4 colorAdd = vec4(colorScale - vec4(1.0, 1.0, 1.0, 1.0));
    gl_FragColor = ((IMG_THIS_PIXEL(inputImage) * colorScale) + colorAdd);
}