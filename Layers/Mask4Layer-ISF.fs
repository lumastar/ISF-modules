/*{
	"DESCRIPTION": "Map 4 layers of content into a mask layer. Scaling and translation can be applied to the mask. The mask transformations can also be proportionately applied to the content layers.",
    "CREDIT": "Written by Wil Squires for EDWARDOtme.",
	"INPUTS":
    [
        {
        "NAME": "mask",
        "TYPE": "image"
        },
        {
        "NAME": "layer1",
        "TYPE": "image"
        },
        {
        "NAME": "layer2",
        "TYPE": "image"
        },
        {
        "NAME": "layer3",
        "TYPE": "image"
        },
        {
        "NAME": "layer4",
        "TYPE": "image"
        },
        {
            "NAME": "mapMode",
            "LABEL": "Map Mode",
            "TYPE": "bool"
        },
        {
            "NAME": "scaleXY",
            "LABEL": "Scale XY",
            "TYPE": "float",
            "DEFAULT": 1.0,
            "MIN": 0.0
        },
        {
            "NAME": "translateX",
            "LABEL": "Translate X",
            "TYPE": "float",
            "DEFAULT": 0.0
        },
        {
            "NAME": "translateY",
            "LABEL": "Translate Y",
            "TYPE": "float",
            "DEFAULT": 0.0
        },
        {
            "NAME": "masterBrightness",
            "LABEL": "Master Brightness",
            "TYPE": "float",
            "DEFAULT": 1.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "red",
            "LABEL": "Red",
            "TYPE": "float",
            "DEFAULT": 1.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "green",
            "LABEL": "Green",
            "TYPE": "float",
            "DEFAULT": 1.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "blue",
            "LABEL": "Blue",
            "TYPE": "float",
            "DEFAULT": 1.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "transparencyLayer1",
            "LABEL": "Transparency Layer 1",
            "TYPE": "float",
            "DEFAULT": 1.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "transparencyLayer2",
            "LABEL": "Transparency Layer 2",
            "TYPE": "float",
            "DEFAULT": 1.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "transparencyLayer3",
            "LABEL": "Transparency Layer 3",
            "TYPE": "float",
            "DEFAULT": 1.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "transparencyLayer4",
            "LABEL": "Transparency Layer 4",
            "TYPE": "float",
            "DEFAULT": 1.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "colorLayer1",
            "LABEL": "Color Layer 1",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "colorLayer2",
            "LABEL": "Color Layer 2",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "colorLayer3",
            "LABEL": "Color Layer 3",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "colorLayer4",
            "LABEL": "Color Layer 4",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "transformLayer1",
            "LABEL": "Transform Layer 1",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "transformLayer2",
            "LABEL": "Transform Layer 2",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "transformLayer3",
            "LABEL": "Transform Layer 3",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": 0.0,
            "MAX": 1.0
        },
        {
            "NAME": "transformLayer4",
            "LABEL": "Transform Layer 4",
            "TYPE": "float",
            "DEFAULT": 0.0,
            "MIN": 0.0,
            "MAX": 1.0
        }
	]
 }*/

vec2 transform(vec2 transformedCoord)
{
    // Move origin to centre and scale based on aspect ratio
    transformedCoord -= vec2(0.5, 0.5);

    // Calculate aspect ratio
    float aspect = (RENDERSIZE.x / RENDERSIZE.y);

    // Scale down based on aspect ratio
    transformedCoord *= vec2(aspect, 1.0);

    // Translate
    transformedCoord += vec2((translateX * -0.5), (translateY * -0.5));

    // Scale
    transformedCoord *= vec2((1.0 / scaleXY), (1.0 / scaleXY));

    // Scale back up
    transformedCoord *= vec2((1.0 / aspect), 1.0);

    // Reset origin
    transformedCoord += vec2(0.5, 0.5);

    return transformedCoord;
}

void main()
{
    // Get coordinate of textures pixels to use
    vec2 pixelCoords = vv_FragNormCoord;
    vec2 pixelCoordsTransformed = transform(pixelCoords);

    // Prevent mask texture from repeating
    vec4 pixelColorMask;
    if (pixelCoordsTransformed.x > 1.0 || pixelCoordsTransformed.x < 0.0 || pixelCoordsTransformed.y > 1.0 || pixelCoordsTransformed.y < 0.0)
        pixelColorMask = vec4(0.0, 0.0, 0.0, 0.0);
    else
    	pixelColorMask = IMG_NORM_PIXEL(mask, pixelCoordsTransformed);

    vec4 pixelColor;

    if (mapMode)
    {
        // Calculate coordinate of mask texture to use for englarged mask
        vec2 pixelCoordsTransformedMapMode = pixelCoordsTransformed;
        pixelCoordsTransformedMapMode -= vec2(0.5, 0.5);
        pixelCoordsTransformedMapMode *= vec2(0.9, 0.9);
        pixelCoordsTransformedMapMode += vec2(0.5, 0.5);
        
        // Prevent mask texture from repeating
        if (pixelCoordsTransformedMapMode.x > 1.0 || pixelCoordsTransformedMapMode.x < 0.0 || pixelCoordsTransformedMapMode.y > 1.0 || pixelCoordsTransformedMapMode.y < 0.0)
            pixelColor = vec4(0.0, 0.0, 0.0, 0.0);
        else
        	pixelColor = IMG_NORM_PIXEL(mask, pixelCoordsTransformedMapMode);

        // Compose with inverse of original mask
        pixelColor *= (1.0 - pixelColorMask);
        pixelColor.w = pixelColorMask.w;
    }
    else
    {
        // Get pixel for each layer
        vec2 pixelCoordsDifference = pixelCoordsTransformed - pixelCoords;
        
        vec2 pixelCoordsLayer4 = pixelCoords + (pixelCoordsDifference * transformLayer4);
		vec4 pixelColorLayer4 = IMG_NORM_PIXEL(layer4, pixelCoordsLayer4);

        vec2 pixelCoordsLayer3 = pixelCoords + (pixelCoordsDifference * transformLayer3);
        vec4 pixelColorLayer3 = IMG_NORM_PIXEL(layer3, pixelCoordsLayer3);

        vec2 pixelCoordsLayer2 = pixelCoords + (pixelCoordsDifference * transformLayer2);
        vec4 pixelColorLayer2 = IMG_NORM_PIXEL(layer2, pixelCoordsLayer2);

        vec2 pixelCoordsLayer1 = pixelCoords + (pixelCoordsDifference * transformLayer1);
        vec4 pixelColorLayer1 = IMG_NORM_PIXEL(layer1, pixelCoordsLayer1);

        // Apply colour filter for each layer
        // Calculate the filtered color values, find the difference between the filtered and
        // original values, scale this difference value by the amount that the layer's color
        // should be filtered, then subtract these value from the actual color values.
        vec4 color = vec4(red, green, blue, 1.0);

        pixelColorLayer4 -= ((pixelColorLayer4 - (pixelColorLayer4 * color)) * colorLayer4);

        pixelColorLayer3 -= ((pixelColorLayer3 - (pixelColorLayer3 * color)) * colorLayer3);

        pixelColorLayer2 -= ((pixelColorLayer2 - (pixelColorLayer2 * color)) * colorLayer2);

        pixelColorLayer1 -= ((pixelColorLayer1 - (pixelColorLayer1 * color)) * colorLayer1);

        // Apply transparency to each layer
        pixelColorLayer4 *= transparencyLayer4;
        pixelColorLayer3 *= transparencyLayer3;
        pixelColorLayer2 *= transparencyLayer2;
        pixelColorLayer1 *= transparencyLayer1;

        // Compose Layers
        pixelColor = pixelColorLayer4;
        float alpha = 1.0 - pixelColorLayer4.w;

        pixelColor += pixelColorLayer3 * alpha;
        alpha *= 1.0 - pixelColorLayer3.w;

        pixelColor += pixelColorLayer2 * alpha;
        alpha *= 1.0 - pixelColorLayer2.w;

        pixelColor += pixelColorLayer1 * alpha;

        // Apply master brightness
        pixelColor *= masterBrightness;

        // Compose with mask
        pixelColor *= pixelColorMask;
    }

    gl_FragColor = pixelColor;
}