# EDWARDOtme ISF modules

This is a collection of custom OpenGL image processing modules written using the Interactive Shader Format standard. 

These are made for EDWARDOtme use with the music visual application Magic, but they are compatible with any application that supports the ISF standard.

For more information on ISF see: http://vdmx.vidvox.net/blog/isf and https://www.interactiveshaderformat.com/spec

For more information on Magic see: https://magicmusicvisuals.com/

## Naming

### Modules

Each module is named to concisely describe what function it performs or effect is has on input.

Module names include the term "ISF" so that when used in other applications it is clear they use the Interactive Shader Format. This can be helpful to know when combining with other non-ISF modules as they may operate differently. For example in Magic the standard Transform modules operate in 3D whereas all ISF modules flatten input to a 2D image.

Some modules have two versions, one using just a fragment shader and one using a fragment and vertex shader. In such cases the names will include the terms "FS" and "VS FS" to indicate which implementation is being used in host applications.

### Folders

Folders are named following the conventions used in Magic.
